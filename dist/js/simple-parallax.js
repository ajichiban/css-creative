console.log('inicio');
let bg = document.querySelector('#bg'),
    moon = document.querySelector('#moon'),
    mountain = document.querySelector('#mountain'),
    road = document.querySelector('#road'),
    text = document.querySelector('#text')

    console.log('luna : ', moon );

	window.addEventListener('scroll', function(){

		let scrollYValue = window.scrollY;

		bg.style.top = scrollYValue * 0.5 + 'px'; 
		moon.style.left = -scrollYValue * 0.5 + 'px'; 
		mountain.style.top = -scrollYValue * 0.15 + 'px'; 
		road.style.top = scrollYValue * 0.15 + 'px';
		text.style.top = scrollYValue * 1 + 'px'

	})